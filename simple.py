#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

"""
@version: v1.0
@author: pengshp
@license: Apache Licence 
@contact: pengshp@gmail.com
@site: https://pengshp.github.io
@software: PyCharm
@file: simple.py
@instructions: 爬去瓜子二手车网数据
@time: 2017/8/31 上午2:32
"""
import requests
from bs4 import BeautifulSoup

header = {
    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.101 Safari/537.36'}


def detaiOper(url):
    web_data = requests.get(url, header)
    soup = BeautifulSoup(web_data.text, 'lxml')
    titles = soup.select('.t')
    prices = soup.select('p')
    for title, price in zip(titles, prices):
        data = {
            'title': title.get_text(),
            'detailHerf': title.get('href'),
            'price': price.get_text().replace(u'万', '').replace(' ', '')
        }
        print(data)


def start():
    urls = ['https://www.guazi.com/tj/buy/o{}/'.format(str(i)) for i in range(1, 30, 1)]
    for url in urls:
        detaiOper(url)


if __name__ == '__main__':
    start()
