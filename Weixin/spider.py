#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

"""
@version: v1.0
@author: pengshp
@license: Apache Licence 
@contact: pengshp@gmail.com
@site: https://pengshp.github.io
@software: PyCharm
@file: spider.py
@instructions: 
@time: 2017/8/31 下午4:56
"""
import requests
from urllib.parse import urlencode
from requests.exceptions import ConnectionError
import pymongo

client=pymongo.MongoClient('localhost')
db=client['weixin']

base_url = 'http://weixin.sogou.com/weixin?'

headers = {
    'Cookie': 'SUV=00BA71D478ECAE925937BEA699655979; IPLOC=CN4401; SUID=92AEEC783320910A0000000059A7CB35; ABTEST=0|1504169210|v1; SNUID=B18DC85B24267B2B8B584CCF24DC2823; weixinIndexVisited=1; sct=1; JSESSIONID=aaamlF6488Y2d7rEgJ24v; ppinf=5|1504170795|1505380395|dHJ1c3Q6MToxfGNsaWVudGlkOjQ6MjAxN3x1bmlxbmFtZTo2OlZpY2VudHxjcnQ6MTA6MTUwNDE3MDc5NXxyZWZuaWNrOjY6VmljZW50fHVzZXJpZDo0NDpvOXQybHVPLUdnLW5zRDhaM0dVdjJsZXYwVVJZQHdlaXhpbi5zb2h1LmNvbXw; pprdig=dEs-3VwMNVA60x1AaGrmaEr1mEEg1Z0oh3n_IP426aXZze5SqxPb6epBZ7nGmgcQ_-D9dwlPftCoS3CUhB_ZVMFeGxGgJhqiifgy912vlR-r-QLHiztQHgmfkSi9xHY_eldj6Jk31uqml5GXywtBCGeDJ_yTN3DRtFLONVhM8mU; sgid=28-30668971-AVmn0yuaqdnz93GG36v7whk; ppmdig=1504170795000000b7045d62c781969a72603a084405f4a3',
    'Host': 'weixin.sogou.com',
    'Upgrade-Insecure-Requests': '1',
    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36'
}


def get_html(url):
    try:
        response = requests.get(url, headers=headers, allow_redirects=False)  # 禁止自动跳转
        if response.status_code == 200:
            return response.text
        if response.status_code == 302:
            print("302")

    except ConnectionError:
        return get_html(url)


def get_index(keyword, page):
    data = {
        'query': keyword,
        'type': 2,  # 2表示文章
        'page': page
    }
    queries = urlencode(data)
    url = base_url + queries
    html = get_html(url)
    return html

def save_to_mongo(data):
    if db['articles'].update({'title': data['title']},{'$set':data},True):
        print("Save to Mongo",data['title'])
    else:
        print('Save to Mongo Failed',data['title'])

def main(keyword):
    for page in range(1, 100):
        html = get_index(keyword, page)
        print(html)

def parse_index(html):


if __name__ == '__main__':
    main('python')
